![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Project Foghorn water and power-to-gas siting web application, by Team SpeakClearly, for the 2019 IoT World Hackathon.

Register: https://climathon.climate-kic.org/en/san-francisco 
(was 
https://www.eventbrite.com/e/iot-world-hackathon-2019-tickets-56442365635 )

Project description: "pfwp2gs"
  https://twitter.com/jsalsman/status/1127671097426493440
(see parent tweets)

**JSFiddle:** https://jsfiddle.net/jsalsman/0zughLbt 
**PLEASE NOTE:** currently development is taking place on the JSFiddle, 
where the TODO list will be moved to GitLab issues soon, and to which 
collaboration will transition when we outpace JSfiddle collaboration.

Copyright 2019 by James Salsman. This project is licensed under the
GNU Affero General Public License v3.0. 

---

Initial jsfiddle source code (HTML, Javascript, CSS)

```

<!--
Project Foghorn water and power-to-gas siting web application, by Team SpeakClearly, for the 2019 IoT World Hackathon.

Register:
https://www.eventbrite.com/e/iot-world-hackathon-2019-tickets-56442365635

Project description: "pfwp2gs"
  https://twitter.com/jsalsman/status/1127671097426493440
(see parent tweets)

GitLab: https://gitlab.com/speakclearly/pfwp2gs

Copyright 2019 by James Salsman. This project is licensed under the
GNU Affero General Public License v3.0.

  Bootstrap docs: https://getbootstrap.com/docs
-->
<script src="https://d3js.org/d3.v4.min.js"></script>
<script src="https://d3js.org/d3-selection-multi.v0.4.min.js"></script><!-- https://iros.github.io/d3-v4-whats-new/#26 -->
<script src="https://unpkg.com/d3@5"></script>
<script src="https://unpkg.com/topojson-client@3"></script><!-- https://bl.ocks.org/mbostock/19ffece0a45434b0eef3cc4f973d1e3d
<script src="https://d3js.org/topojson.v2.min.js"></script> -->

<!-- TODO:
1. Finish design
2. MVP development
3. Test different optimizations during demo
4. Setup WeFunder
5. Convert from JavaScript to client-side Python,
   e.g. http://transcrypt.org
-->

<div class="container">
  <div class="row">
    <div class="col">
      Project Foghorn water and power-to-gas siting web
      application
    </div>
    <div class="col">
      by Team SpeakClearly,<br/>2019 IoT World Hackathon
    </div>
  </div>
  <div class="row">
    <div class="col">
      Main menu: <u>Map</u>, <u>Tables</u>, <u>Optimize</u>,
      <u>Preferences</u>, <u>Save</u>, <u>Load</u>,
      <u>Submit plan</u>.
    </div>
    <div class="col">
      Current status: design in progress; development,
      optimizations, and WeFunder pending
    </div>
    <div class="col">
      Goal: 600% US carbon emission reduction,
      avoid Jevons' Paradox, create ______ new jobs
    </div>
  </div>
  <div class="row">
    <svg width="100%" height="250"></svg>
  </div>
</div>



// https://iros.github.io/d3-v4-whats-new/#95

var svg = d3.select("svg"),
    width = window.innerWidth,
    height = +svg.attr("height");

d3.json("https://raw.githubusercontent.com/gist/mbostock/4090846/raw/07e73f3c2d21558489604a0bc434b3a5cf41a867/us.json").then(function(us) {

  var conus = topojson.feature(us, {
    type: "GeometryCollection",
    geometries: us.objects.states.geometries.filter(function(d) {
      return d.id !== 2 // AK
        && d.id !== 15 // HI
        && d.id < 60; // outlying areas
    })
  });

  // ESRI:102004
  var path = d3.geoPath()
      .projection(d3.geoConicConformal()
          .parallels([33, 45])
          .rotate([96, 0])
          .center([0, -39])
          .fitSize([width, height], conus));

  svg.append("path")
      .datum(conus)
      .attr("d", path);

});



.row {
  background: #f8f9fa;
  margin-top: 20px;
}

.col {
  border: solid 1px #6c757d;
  padding: 10px;
}

```

---

## The remainder of this file is template boilerplate
-- and so should probably be ignored if you are working on the hackathon.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your HTML files in the `public/` directory.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means that you have wrongly set up the CSS URL in your
   HTML files. Have a look at the [index.html] for an example.

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
